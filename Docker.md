
# Network

## modes de fonctionnement

### bridge

- par défaut (reseau `bridge` de docker, interface docker0)
- créer un reseau virtuel configurable 
- Connection possible avec l'exterieur et avec le resau bridge
- possibilité de créer un reseau isolé (trafic vers l'exterieur, option `--internal` à la création)
- possibilité de controler le trafic sur les reseaux bridgen avec iptables (même le trafic sur le reseau passe par la machine hote (et est donc filtré))
- il est possible de connecter un conteneur a plusieurs reseaux via la commande `docker network attach

### host

- le conteneur se connecte directement via les interfaces de la machine hote
- Utile pour faire des tests mais dangereux pour une utilisation "réelle"

### macvlan

- permet (visiblement) de connecter le conteneur aux meme reseaux que la machine hote mais avec une adresse mac differente


### none

- pas de réseau sur le conteneur
- le conteneur à accés à une loopback

## Iptables

- Docker utilise par défaut des régles iptables pour gérer le trafic du reseau
- Une chaine est reservée pour les régles des utilisateurs : elles sont traitées avant les regles automatiques de docker

## DNS

- herité par defaut de la config du PC
- peut etre mis a la main avec l'option `--dns` au lancement du conteneur (cf doc)

## Proxy

- peut etre configuré pour la machine hôte : tous les conteneurs reprendront la configuration (cf doc)

# Partage de fichiers

- Option `readonly` donne accés uniqument en lecture. Util pour utiliser du code recuperé sur la machine hôte et executé par plusieurs conteneurs

- Deux points doivent être vérifiés lorsqu’on autorise l’accès à un stockage extérieur par un conteneur au travers d’un volume. Tout d’abord, les droits d’accès et en particulier l’utilisateur avec lequel le conteneur est exécuté qui va déterminer ce à quoi le conteneur pourra avoir accès, mais également l’étendue du volume que l’on exporte dans le conteneur.

## Volume 

- fait pour le partage de fichiers entre plusieurs conteneurs
- stockage sur la machine hôte dans le repertoire `/var/lib/docker/volumes/*nom_du_volume*/_data`
- si on utilise un dossier du container qui n'est pas vide, le contenu du dossier et copié dans le volume

## Bind mount

- permet de partager n'importe quel fichier du système de fichier de la machine hôte
- les fichiers déjà existant sont "cachés" (attention, peut mener a des comportement etranges ou a des crashs)
- souvent utilisé pour partager des fichiers de configuration ou le code de l'application a lancer

# Machine hotes

## pas lié a docker

- L'acces sur la machine hote à root ou tout autre utilisateur du groupe docker compromet tous les conteneurs. La machine doit donc etre proteger avec des mesures classiques


## Lié à docker

- DNS des conteneurs : hérité de la machine hôte, mais paut etre configuré. Ajout de 8.8.8.8 si aucune des adresses de DNS passé ne marche

### cgroup
 - Les cgroups sont des mesures d'isolation. C'est un ensemble de processus auxquels sont appliqués un jeu de paramètres concernant les ressources accessibles du système d’exploitation et permettant de contrôler les ressources utilisables par un processus.

### namespace
- Les namespaces (ou espaces de nommage) sont des mécanismes d’isolation des processus restreignant leur vue respective du système hôte

### seccomp
- Seccomp offre la possibilité de restreindre les appels systèmes que peuvent utiliser les processus et limite par la même occasion l’exploitabilité des failles les mettant en jeu.

### capabilities
- Ils ont pour but de subdiviser les pouvoirs du super utilisateur root, notamment pour restreindre les conséquences de la compromission d’un programme privilégié.

### Monitory Access Control
 - Les systèmes de MAC permettent la création de profils destinés aux gestionnaires de conteneurs sous forme de liste blanche d’autorisations d’accès à des ressources du système, telles que le système de fichiers ou les interfaces réseau. exemple: SElinux, AppArmor

# Conteneurs

## images

### création des images

- ne pas laisser les droits root sur le conteneurs. la plupart des images officiel d'os de base (ubuntu, debian, alpine, ...) donne un acces root dans le conteneur. Il vaut mieux le changer dans les images qu'on créer à l'aide de la commande 

- Le user  root à l'intérieur d'un conteneur est le même que celui du système hôte. Ainsi, l'exécution en tant que root d'un processus conteneurisé ayant été compromis  peut à son tour  compromettre l'hôte Docker. Une bonne pratique est  d'avoir un utilisateur unique pour chaque conteneur permettant à un administrateur d'attribuer l'ensemble minimal de Capacités Linux requises pour un processus particulier. 

### dépot des images



### signature et verification
 - la fonction Docker Content Trust garantit que les images, les signatures et les hachages soient vérifiés avant utilisation et nécessitent que les utilisateurs signent leurs propres images en tant que bien.
## "instaciation" des conteneurs




